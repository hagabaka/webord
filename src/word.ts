type Letter = string;

export function lettersOfWord(word: string): Letter[] {
  return [...word];
}

export function adjacentLettersOfWord(word: string): [Letter, Letter][] {
  const letters = lettersOfWord(word);
  if(letters.length < 2) {
    return [];
  }
  const [first, second, ...rest] = letters;
  return [[first, second], ...adjacentLettersOfWord([second, ...rest].join(''))];
}
