import shuffle from "just-shuffle";

export function makePalette(letters: string[]) {
  const pool = shuffle(letters);
  return (letter: string) => {
    const fraction = pool.indexOf(letter) / pool.length;
    return `hsl(${fraction * 360}, 40%, 80%)`;
  }
}