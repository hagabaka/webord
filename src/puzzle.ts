import { Graph } from "./graph";
import { JS, NFA, CharSet } from "refa";

const maxCharacter = 0xFFFF;

function charSet(letter: string) {
  return CharSet.fromCharacters(maxCharacter, [letter.charCodeAt(0)]);
}

export function makeChecker(graph: Graph) {
  const builder = new NFA.Builder(new NFA.LimitedNodeFactory());
  const nodesByLetter: Record<string, NFA.Node> = {};
  for(const {letter} of graph.nodes) {
    const node = nodesByLetter[letter] = builder.createNode();
    builder.makeFinal(node);
    builder.initial.link(node, charSet(letter));
  }
  for(const {nodes: [{letter: first}, {letter: second}]} of graph.edges) {
    nodesByLetter[first].link(nodesByLetter[second], charSet(second));
    nodesByLetter[second].link(nodesByLetter[first], charSet(first));
  }
  const nfa = NFA.fromBuilder(builder, {maxCharacter});
  const {source, flags} = JS.toLiteral(nfa.toRegex({maxNodes: 20000}));
  return new RegExp(`^(?:${source})$`, flags);
}