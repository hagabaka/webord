import { createPipe, flatMap, map, uniqBy, mapToObj, difference, prop } from "remeda";
import { adjacentLettersOfWord, lettersOfWord } from "./word";

type Node = {
  letter: string;
  key: Key;
}
type Edge = {
  nodes: [Node, Node];
  key: Key;
}
type Key = string;

export interface Graph {
  nodes: Node[];
  edges: Edge[];
}

function makeNode(letter: string): Node {
  return {
    letter,
    key: letter,
  };
}

function makeEdge(pair: [string, string]): Edge {
  const [first, second] = [...pair].sort();
  return {
    nodes: [makeNode(first), makeNode(second)],
    key: `${first}-${second}`,
  }
}

export function getGraph(words: string[]): Graph {
  return {
    nodes: createPipe(
      flatMap(lettersOfWord),
      map(makeNode),
      uniqBy(prop('key'))
    )(words),
    edges: createPipe(
      flatMap(adjacentLettersOfWord),
      map(makeEdge),
      uniqBy(prop('key'))
    )(words),
  }
}

interface Renderer {
  addNodes(nodes: Node[]): void;
  addEdges(edges: Edge[]): void;
  removeNodesWithKeys(keys: Key[]): void;
  removeEdgesWithKeys(keys: Key[]): void;
  nodeKeys: Key[];
  edgeKeys: Key[];
}

export function updateRenderer(graph: Graph, renderer: Renderer) {
  const nodesByKey = mapToObj(graph.nodes, node => [node.key, node]);
  const nodeKeys = graph.nodes.map(prop('key'));
  const edgesByKey = mapToObj(graph.edges, edge => [edge.key, edge]);
  const edgeKeys = graph.edges.map(prop('key'));
  renderer.removeEdgesWithKeys(difference(renderer.edgeKeys, edgeKeys));
  renderer.removeNodesWithKeys(difference(renderer.nodeKeys, nodeKeys));
  renderer.addNodes(difference(nodeKeys, renderer.nodeKeys).map(key => nodesByKey[key]));
  renderer.addEdges(difference(edgeKeys, renderer.edgeKeys).map(key => edgesByKey[key]));
}
