import random from "just-random";
import { difference } from "remeda";
import type { Ref } from "vue";

// Throws on empty input instead of returning undefined
export function unsafeRandom<T>(items: T[]): T {
  const [first, ...rest] = items;
  return random([first, ...rest]);
}

export function addTo<T>(list: Ref<T[]>, item: T) {
  list.value = [...list.value, item];
}

export function removeFrom<T>(list: Ref<T[]>, item: T) {
  list.value = difference(list.value, [item]);
}