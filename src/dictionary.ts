import { englishAll as words } from 'wordlist-js'
import { unsafeRandom as random } from './util';

export const minimumWordLength = 4;
const validWords = words.filter(word => word.length >= minimumWordLength);

export function isWord(word: string): boolean {
  return validWords.includes(word);
}

export function getRandomWord(): string {
  return random(validWords as [string, ...string[]]);
}

export { validWords as words };